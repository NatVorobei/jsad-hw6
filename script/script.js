// Асинхронність в Javascript означає, що деякі операції можуть виконуватись паралельно в фоновому режимі, не блокуючи дію інших операцій. Для асинхронного виконання коду в JS використовуються колбеки, setTimeout(), Promise, async await.

let ipBtn = document.querySelector('.ip_btn');
let ipInfo = document.querySelector('.ip_info');

async function getIP(){
    try{
        let response = await fetch('https://api.ipify.org/?format=json');
        let data = await response.json();
        let ip = data.ip;
        // console.log(ip);
        return ip;
    } catch (err) {
        console.log(err);
    }  
}

async function getLocation(ip){
    try{
        let response = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,region`);
        let data = await response.json();
        console.log(data);
        return data;
    } catch (err) {
        console.log(err);
    }
}


function displayResults(location){
    ipInfo.innerHTML = `
        <p>Continent: ${location.continent}</p>
        <p>Country: ${location.country}</p>
        <p>Region Name: ${location.regionName}</p>
        <p>Region: ${location.region}</p>
        <p>City: ${location.city}</p>
    `;
}

ipBtn.addEventListener('click', async () => {
    let ip = await getIP();
    let location = await getLocation(ip);
    displayResults(location);
});